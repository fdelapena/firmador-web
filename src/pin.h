/* Firmador web is a program that communicates web browsers with smartcards.

Copyright (C) 2017, 2022 Francisco de la Peña Fernández.

This file is part of Firmador web.

Firmador web is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firmador web is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firmador web.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef FIRMADOR_WEB_PIN_H
#define FIRMADOR_WEB_PIN_H

#include <cstddef>

int pin_callback(void *userdata, int attempt, const char *token_url, const char *token_label, unsigned int flags, char *pin, std::size_t pin_max);

#endif
