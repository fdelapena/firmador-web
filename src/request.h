/* Firmador web is a program that communicates web browsers with smartcards.

Copyright (C) 2017, 2022 Francisco de la Peña Fernández.

This file is part of Firmador web.

Firmador web is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firmador web is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firmador web.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef FIRMADOR_WEB_REQUEST_H
#define FIRMADOR_WEB_REQUEST_H

#include "firmador-web.h"

#include <cstddef>

#include <microhttpd.h>

#define FIRMADOR_WEB_PORT 9795

#if MHD_VERSION >= 0x00097002
typedef enum MHD_Result MHD_RESULT;
#else
typedef int MHD_RESULT;
#endif

class FirmadorWeb;

MHD_RESULT request_callback(void *cls, struct MHD_Connection *connection, const char *url, const char *method, const char *version, const char *upload_data, std::size_t *upload_data_size, void **con_cls);

#endif
