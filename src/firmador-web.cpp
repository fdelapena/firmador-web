/* Firmador is a program that communicates web browsers with smartcards.

Copyright (C) 2017, 2022 Francisco de la Peña Fernández.

This file is part of Firmador web.

Firmador web is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firmador web is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firmador web.  If not, see <http://www.gnu.org/licenses/>.  */

#include "firmador-web.h"
//#include "base64.h"
#include "certificate.h"
#include "pin.h"
#include "server.h"

//#include <iostream>
//#include <string>
#include <sstream>

#include <gnutls/abstract.h>
//#include <wx/filesys.h>
//#include <wx/fs_inet.h>
//#include <wx/sstream.h>

IMPLEMENT_APP(FirmadorWeb)

bool FirmadorWeb::OnInit() {
	this->Connect(wxID_ANY, wxEVT_CERTIFICATES, wxCommandEventHandler(FirmadorWeb::GetCertificates));
	server_start(this);

	gnutls_pkcs11_set_pin_function(pin_callback, NULL);

	int ret = gnutls_pkcs11_init(GNUTLS_PKCS11_FLAG_MANUAL, NULL);
	if (ret < GNUTLS_E_SUCCESS) {
		std::ostringstream error;
		error << "Error al inicializar el subsistema de PKCS #11:" << std::endl << gnutls_strerror(ret);
		wxMessageBox(wxString(error.str().c_str(), wxConvUTF8), wxT("Error al inicializar subsistema"), wxICON_ERROR);
		exit(ret);
	}

	std::ostringstream path;
#ifdef __WIN32__
	path << getenv("WINDIR") << "\\System32\\asepkcs.dll";
#elif __WXOSX_MAC__
	path << "/Library/Application Support/Athena/libASEP11.dylib";
#elif __LINUX__
	path << "/usr/lib/x64-athena/libASEP11.so";
#else
	wxMessageBox(wxString("Sistema no soportado por el firmador web.\nEl fabricante de la tarjeta solamente soporta GNU/Linux, macOS y Windows con procesadores x86 y x86_64.", wxConvUTF8), wxT("Sistema no soportado"), wxICON_ERROR);
	exit(1);
#endif
	ret = gnutls_pkcs11_add_provider(path.str().c_str(), NULL);
	if (ret < GNUTLS_E_SUCCESS) {
		std::ostringstream error;
		error << "Error al agregar proveedor:" << std::endl << gnutls_strerror(ret);
		wxMessageBox(wxString(error.str().c_str(), wxConvUTF8), wxT("Error al agregar proveedor"), wxICON_ERROR);
		exit(ret);
	}

	get_certificates();

	gnutls_privkey_t key;
	ret = gnutls_privkey_init(&key);
	if (ret < GNUTLS_E_SUCCESS) {
		std::ostringstream error;
		error << "Error al inicializar la clave privada:" << std::endl << gnutls_strerror(ret);
		wxMessageBox(wxString(error.str().c_str(), wxConvUTF8), wxT("Error al inicializar clave"), wxICON_WARNING);
		return ret;
	}

	/*
	 * Tras seleccionarse, cargar el identificador correspondiente, esta
	 * vez con PIN para poder usar la clave privada para poder firmar.
	 */
/*
	ret = gnutls_privkey_import_url(key, cert_choices.Item(choiceDialog.GetSelection()).mb_str(wxConvUTF8), 0);
	if (ret < GNUTLS_E_SUCCESS) {
		std::ostringstream error;
		error << "Error al importar la URL de la clave privada:" << std::endl << gnutls_strerror(ret);
		wxMessageBox(wxString(error.str().c_str(), wxConvUTF8), wxT("Error al importar URL de clave"), wxICON_WARNING);
		return ret;
	}

	// FIXME: obtener datos desde /rest/sign
	std::string datos_base64 =
		"MYIBUzAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMC8GCSqGSIb3DQEJBDEiBCDA"
		"LgEEhMr4mMg9m1MJ1vQBKXm/BcZrdi7E1GJaN6Nd2DCCAQQGCyqGSIb3DQEJEAIv"
		"MYH0MIHxMIHuMIHrMA0GCWCGSAFlAwQCAQUABCDYNjvWvS/jDEGRzVuwWpfnVW7+"
		"AfIHxFnYHexYGGv2ZTCBtzCBn6SBnDCBmTEZMBcGA1UEBRMQQ1BKLTQtMDAwLTAw"
		"NDAxNzELMAkGA1UEBhMCQ1IxJDAiBgNVBAoTG0JBTkNPIENFTlRSQUwgREUgQ09T"
		"VEEgUklDQTEiMCAGA1UECxMZRElWSVNJT04gU0lTVEVNQVMgREUgUEFHTzElMCMG"
		"A1UEAxMcQ0EgU0lOUEUgLSBQRVJTT05BIEZJU0lDQSB2MgITFAABH/a5gZb8gqHY"
		"/AAAAAEf9g==";
	std::string datos = base64_decode(datos_base64);
	gnutls_datum_t data = {(unsigned char*)datos.c_str(), (unsigned)datos.length()};

	gnutls_datum_t sig;
	ret = gnutls_privkey_sign_data(key, GNUTLS_DIG_SHA256, 0, &data, &sig);
	if (ret < GNUTLS_E_SUCCESS) {
		std::ostringstream error;
		error << "Error al firmar:" << std::endl << gnutls_strerror(ret);
		wxMessageBox(wxString(error.str().c_str(), wxConvUTF8), wxT("Error al firmar"), wxICON_WARNING);
		return ret;
	}

	gnutls_datum_t signatureValue;
	gnutls_pem_base64_encode_alloc(NULL, &sig, &signatureValue);
	std::cout << "signatureValue: " << signatureValue.data << std::endl;
	gnutls_free(sig.data);
*/

/*
	// TODO: get URIs from AIA
	wxFileSystem::AddHandler(new wxInternetFSHandler());
	wxFileSystem fs;
	//http://fdi.sinpe.fi.cr/repositorio/CA%20SINPE%20-%20PERSONA%20FISICA%20v2.crt
	//http://www.firmadigital.go.cr/repositorio/CA%20POLITICA%20PERSONA%20FISICA%20-%20COSTA%20RICA.crt
	//http://www.firmadigital.go.cr/repositorio/CA%20RAIZ%20NACIONAL%20-%20COSTA%20RICA%20v2.crt
	// FIXME: don't use wxStringOutputStream because it does not support binary files (DER)
	wxFSFile* file = fs.OpenFile(wxT("http://fdi.sinpe.fi.cr/repositorio/CA%20SINPE%20-%20PERSONA%20FISICA%20v2.crt"));
	if (file) {
		wxInputStream *inputStream = file->GetStream();
		if (inputStream && inputStream->IsOk()) {
			wxString buffer;
			wxStringOutputStream outputStream(&buffer);
			inputStream->Read(outputStream);
			std::cout << "buffer: " << std::string(buffer.mb_str()) << std::endl;
		}
	}
*/

	gnutls_privkey_deinit(key);
	gnutls_pkcs11_deinit();

	return true;
}

void FirmadorWeb::GetCertificates(wxCommandEvent& WXUNUSED(event)) {
	wxMessageBox(wxT("Stub."), wxT("Stub"), wxICON_ERROR);
	std::cout << "stub!" << std::endl;
	return;
}
