/* Firmador web is a program that communicates web browsers with smartcards.

Copyright (C) 2017, 2022 Francisco de la Peña Fernández.

This file is part of Firmador web.

Firmador web is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firmador web is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firmador web.  If not, see <http://www.gnu.org/licenses/>.  */

#include "server.h"
#include "request.h"

void server_start(FirmadorWeb *firmadorWeb) {
	struct sockaddr_in daemon_ip_addr;
	memset(&daemon_ip_addr, 0, sizeof(struct sockaddr_in));
	daemon_ip_addr.sin_family = AF_INET;
	daemon_ip_addr.sin_port = htons(FIRMADOR_WEB_PORT);
	daemon_ip_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	struct MHD_Daemon *daemon;
	daemon = MHD_start_daemon(MHD_USE_SELECT_INTERNALLY, FIRMADOR_WEB_PORT, NULL, NULL, &request_callback, firmadorWeb, MHD_OPTION_SOCK_ADDR, &daemon_ip_addr, MHD_OPTION_END);
	if (daemon == NULL) {
		wxMessageBox(wxString("No se ha podido iniciar el servicio firmador web.\nEl puerto podría estar ocupado por otro servicio.", wxConvUTF8), wxT("Error al iniciar"), wxICON_ERROR);
		exit(1);
	}
}
