/* Firmador web is a program that communicates web browsers with smartcards.

Copyright (C) 2017, 2022 Francisco de la Peña Fernández.

This file is part of Firmador web.

Firmador web is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firmador web is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firmador web.  If not, see <http://www.gnu.org/licenses/>.  */

#include "request.h"

#define FIRMADOR_WEB_STRING(s) #s
#define FIRMADOR_WEB_EXPAND_STRING(e) FIRMADOR_WEB_STRING(e)

#include <cstring>
#include <iostream>
#include <string>

MHD_RESULT request_callback(void *cls, struct MHD_Connection *connection, const char *url, const char *method, const char *version, const char *upload_data, std::size_t *upload_data_size, void **con_cls) {
	struct MHD_Response *response;
	MHD_RESULT ret;
	int ret_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
	std::string page = "";
	std::string content_type = "";
	FirmadorWeb *firmadorWeb = static_cast<FirmadorWeb *>(cls);

	(void)version;
	(void)upload_data;
	(void)upload_data_size;
	(void)con_cls;

	if (strcmp(url, "/") == 0 || strcmp(url, "/nexu-info") == 0) {
		ret_code = MHD_HTTP_OK;
		content_type = "application/json;charset=utf-8";
		page = "{ \"version\": \"1.10.5\"}";
	}

	if (strcmp(url, "/nexu.js") == 0) {
		ret_code = MHD_HTTP_OK;
		content_type = "text/javascript; charset=utf-8";
		page = std::string() + "/* @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later */\n"
			"function nexu_get_certificates() {\n"
			"\treq = new XMLHttpRequest();\n"
			"\treq.open('POST', '//localhost:" + FIRMADOR_WEB_EXPAND_STRING(FIRMADOR_WEB_PORT) + "/rest/certificates');\n"
			"\treq.setRequestHeader('Content-Type', 'application/json; charset=utf-8');\n"
			"\treq.send();\n"
			"\treq.onreadystatechange = return_callback;\n"
			"}\n"
			"\n"
			"function nexu_sign_with_token_infos() {\n"
			"\treq = new XMLHttpRequest();\n"
			"\treq.open('POST', '//localhost:" + FIRMADOR_WEB_EXPAND_STRING(FIRMADOR_WEB_PORT) + "/rest/sign');\n"
			"\treq.setRequestHeader('Content-Type', 'application/json; charset=utf-8');\n"
			"\treq.send();\n"
			"\treq.onreadystatechange = return_callback;\n"
			"}\n"
			"\n"
			"function return_callback() {\n"
			"\tif (req.readyState === XMLHttpRequest.DONE) {\n"
			"\t\tif (req.status === 200) {\n"
			"\t\t\treturn req.responseText;\n"
			"\t\t} else {\n"
			"\t\t\treturn req.responseText;\n"
			"\t\t}\n"
			"\t}\n"
			"}\n"
			"/* @license-end */\n";
	}

	if (strcmp(url, "/rest/certificates") == 0) {
		if (strcmp(method, MHD_HTTP_METHOD_OPTIONS) == 0) {
			ret_code = MHD_HTTP_OK;
		}

		if (strcmp(method, MHD_HTTP_METHOD_POST) == 0) {
			ret_code = MHD_HTTP_OK;
			content_type = "application/json; charset=utf-8";
			std::cout << "FIXME: abrir interfaz de certificados" << std::endl;
			std::cout << "FIXME: enviar UUID, ID de certificado, " << "DER del certificado y DER de la cadena." << std::endl;
			page = "{}";
			// FIXME: call an event to work from the main thread
			wxCommandEvent event(wxEVT_CERTIFICATES, wxID_ANY);
			//wxPostEvent(firmadorWeb, event);
			firmadorWeb->GetCertificates(event);
		}
	}

	if (strcmp(url, "/rest/sign") == 0) {
		if (strcmp(method, MHD_HTTP_METHOD_OPTIONS) == 0) {
			ret_code = MHD_HTTP_OK;
		}

		if (strcmp(method, MHD_HTTP_METHOD_POST) == 0) {
			ret_code = MHD_HTTP_OK;
			content_type = "application/json;charset=utf-8";
			std::cout << "FIXME: abrir interfaz de solicitar PIN del token y firmar el PKCS#7 proporcionado con el algoritmo indicado." << std::endl;
			std::cout << "FIXME: enviar firma y algoritmo usado, DER del certificado y DER de la cadena." << std::endl;
		}
	}

	response = MHD_create_response_from_buffer(page.length(), (void*)page.c_str(), MHD_RESPMEM_MUST_COPY);
	if (strcmp(page.c_str(), "") != 0) {
		MHD_add_response_header(response, MHD_HTTP_HEADER_CONTENT_TYPE, content_type.c_str());
	}
	MHD_add_response_header(response, "Access-Control-Allow-Headers", MHD_HTTP_HEADER_CONTENT_TYPE);
	MHD_add_response_header(response, "Access-Control-Allow-Methods", "OPTIONS, GET, POST");
	MHD_add_response_header(response, "Access-Control-Allow-Origin", "*");
	MHD_add_response_header(response, MHD_HTTP_HEADER_CONNECTION, MHD_HTTP_HEADER_CLOSE);
	ret = MHD_queue_response(connection, ret_code, response);
	MHD_destroy_response(response);

	return ret;
}
